// Данная функция подсчитывает сумму всех имеющихся оценок учащихся,
// на вход принимает объект из учащихся, каждый со своей оценкой.
// Имеется проверка передаваемых значений оценок:
// сообщение об ошибке, если значение не является числом,
// либо если является отрицательным числом.

export const sumAllScores = obj => {
    const scores = Object.values(obj);  
    let res = 0;  
    for (let i = 0; i < scores.length; i++) {
        if (typeof scores[i] !== 'number') {
            res = `${scores[i]} не является числом`;
            break;
        }
        if (scores[i] < 0) {
            res = `${scores[i]} меньше 0`;
            break;    
        }

        res = res + scores[i];
    }
    return res;
};




