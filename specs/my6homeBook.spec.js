import { expect, test } from '@jest/globals';
import { logPass, logPassLessNum, logPassNumLet } from '../framework/config/index';
import { allBook, authorized, bookstoreUser, generateToken } from '../framework/services/index';

describe.skip('Проверяем HTTP запросs Bookstore.toolsqa через supertest', () => {
    test('Добавление нового пользователя, валидные данные, получаем userID', async () => {
        // для нового пользователя нужна новая комбинация кредов, см. в UserLogo.js для logPass
        const res = await new bookstoreUser(logPass).postUser();
        console.log(res.body.userID);
        expect(res.status).toEqual(201);
    });

    test('Токен, получение, валидные логин/пароль имеющегося пользователя', async () => {
        const res = await new generateToken(logPass).postLogPass();
        console.log(res.body.token);
        expect(res.status).toEqual(200);
        expect(res.body.token.length).toBeGreaterThanOrEqual(1);
    });

    test('Проверяем авторизацию: валидный пользователь авторизован после получения токена', async () => {
        const res = await new authorized(logPass).postLogPass();
        expect(res.status).toEqual(200);
        expect(res.body).toEqual(true);
    });

    test('Добавление нового пользователя: ошибка, если пользователь уже имеется в системе', async () => {
        const res = await new bookstoreUser(logPass).postUser();
        console.log(res.body);
        expect(res.status).toEqual(406);
    });

    test('Добавление нового пользователя, валидация пароля: ошибка, если только цифры и буквы', async () => {
        const res = await new bookstoreUser(logPassNumLet).postUser();
        expect(res.status).toEqual(400);
    });

    test('Добавление нового пользователя, валидация пароля: ошибка, если меньше 8 символов', async () => {
        const res = await new bookstoreUser(logPassLessNum).postUser();
        expect(res.status).toEqual(400);
    });

    test('Токен, ошибка при получении, если невалидные данные', async () => {
        const res = await new generateToken(logPassLessNum).postLogPass();
        expect(res.status).toEqual(200);
        expect(res.body.token).toEqual(null);
    });

    test('Проверяем, что в списке 3 шт. книг, которые опубликованы в 2020 году (publish_date >= 2020-01-01T00:00:00.000Z)', async () => {
        const { body, status } = await new allBook().getAllBook();
        const r = body.books
            .filter((books) => books.publish_date >= '2020-01-01T00:00:00.000Z')
            .map((books) => books.isbn);
        expect(status).toEqual(200);
        expect(r.length).toEqual(3);
    });

    test('Проверяем, что книги имеются в системе (+ выводим isbn + title всех книг)', async () => {
        const { body, status } = await new allBook().getAllBook();
        expect(status).toEqual(200);
        const r = body.books
            .map((books) => `${books.isbn}, ${books.title}`);
        //console.log(r.[1]);
        expect(body).not.toBe(null);
    });

    test('Проверка, что есть книга с pages = 472', async () => {
        const { body, status } = await new allBook().getAllBook();
        expect(status).toEqual(200);
        const r = body.books
            .find((books) => books.pages === 472);
        console.log(r);
        expect(status).toBeDefined();
    });
});
