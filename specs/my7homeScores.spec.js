import { sumAllScores } from '../src/homework/homework7';
import { expect, test } from '@jest/globals';


describe('Проверка функции, ссума всех оценок учащихся', () => {

    test('1) Проверяем работоспособность, когда в объекте у каждого ученика есть оценки', () => {
        let scores = { Anna: 10, Olga: 1, Ivan: 5, };
        expect(sumAllScores(scores)).toBe(16);
    });

    test('2) Проверяем работоспособность, если в объекте 2 отличника', () => {
        let scores = { Anna: 10, Vlad: 10, };
        expect(sumAllScores(scores)).toBe(20);
    });

    test('3) Проверяем работоспособность, если в объекте ученики с оценкой 0', () => {
        let scores = { Anna: 0, Vlad: 0, Marusja: 0,};
        expect(sumAllScores(scores)).toBe(0);
    });

    test('4) Имеется сообщение об ошибке, если в объекте у учеников только null', () => {
        let scores = { Anna: null, Vlad: null, Marusja: null,};
        expect(sumAllScores(scores)).toEqual('null не является числом');
    });

    test('5) Имеется сообщение об ошибке, если в объекте у одного из учеников null', () => {
        let scores = { Anna: 10, Vlad: 8, Marusja: null, Olga: 5, Viktoria: 7};
        expect(sumAllScores(scores)).toEqual('null не является числом');
    });

    test('6) Имеется сообщение об ошибке, если оценка не является числом, только латиница', () => {
        let scores = { Anna: 'aopfe', Vlad: 8, Marusja: 7, Olga: 5, Viktoria: 7};
        expect(sumAllScores(scores)).toEqual('aopfe не является числом');
    });

    test('7) Имеется сообщение об ошибке, если оценка не является числом, кириллица с цифрами', () => {
        let scores = { Anna: 'э1цуке3', Vlad: 8, Marusja: 7, Olga: 5, Viktoria: 7};
        expect(sumAllScores(scores)).toEqual('э1цуке3 не является числом');
    });

    test('8) Имеется сообщение об ошибке, если несколько оценок не являются числами', () => {
        let scores = { Anna: 'цуке', Vlad: 8, Marusja: 7, Olga: 5, Viktoria: 'wert'};
        expect(sumAllScores(scores)).toEqual('цуке не является числом');
    });

    test('9)  Имеется сообщение об ошибке, если оценка является отрицательным числом', () => {
        let scores = { Anna: -10, Vlad: 8, Marusja: 1, Olga: 5, Viktoria: 7};
        expect(sumAllScores(scores)).toEqual('-10 меньше 0');
    });

});
