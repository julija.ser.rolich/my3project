import { expect, test } from '@jest/globals';
import { apiProvider } from '../framework';
import { validEmail } from '../framework/config/index';

describe.skip('Проверка сервиса с email', () => {
    test.skip('1) Проверяем запрос (ОК), если email валидный', async () => {
        const res = await apiProvider().emailCheck().getAll();
        expect(res.email).toBe(validEmail);
        expect(res.format_valid).toBeTruthy();
    });

    test.skip.each`
  a      | b             | expected
  ${'e'} | ${'@int..com'}| ${false} 
  ${'e'} | ${'int.com'}  | ${false}
  ${'e'} | ${'intcom'}   | ${false}
  ${'e'} | ${'12345'}    | ${false}
  ${'e'} | ${'@intcom'}  | ${false}
  ${'e'} | ${'@'}        | ${false}
  ${'e'} | ${'@.'}       | ${false}
  ${'e'} | ${'@..'}      | ${false}
  ${'e'} | ${'@.com'}    | ${false}
  ${'e'} | ${'@i.1'}     | ${false}
  ${'e'} | ${'@9.1'}     | ${false}
  ${'e'} | ${'@i.1com'}  | ${false}
  ${'e'} | ${'@.com'}    | ${false}
  ${'e'} | ${'@.com.'}   | ${false}
  ${'e'} | ${'.com'}     | ${false}
  ${'.'} | ${'@int.com'} | ${false}
  ${'@'} | ${'@int.com'} | ${false}
    `('2) Проверяем ошибку: $expected, когда $a соединяем с $b', async ({ a, b, expected }) => {
        expect(await apiProvider().emailCheck(a + b).getAll2()).toBe(expected);
    });

    test.skip('2.1) Проверяем ошибку, если email не валидный 1', async () => {
        const res = await apiProvider().emailCheck().getAllnotVal1();
        expect(res.format_valid).not.toBeTruthy();
    });

    test.skip('2.2) Проверяем ошибку, если email не валидный 2', async () => {
        const res = await apiProvider().emailCheck().getAllnotVal2();
        expect(res.format_valid).not.toBeTruthy();
    });

    test.skip('2.3) Проверяем ошибку, если email не валидный 3', async () => {
        const res = await apiProvider().emailCheck().getAllnotVal3();
        expect(res.format_valid).not.toBeTruthy();
    });

    test.skip('3.1) Проверяем ошибку, если запрос БЕЗ указания значения access_key', async () => {
        const res = await apiProvider().emailCheck().getNoAll();
        expect(res.error.code).toEqual(101);
    });

    test.skip('3.2) Проверяем ошибку, если запрос с ПУСТЫМ значением access_key', async () => {
        const res = await apiProvider().emailCheck().getEmpty();
        expect(res.error.code).toEqual(101);
    });

    test.skip('3.3) Проверяем ошибку, если запрос без указания самого параметра access_key', async () => {
        const res = await apiProvider().emailCheck().getNoAccess();
        expect(res.error.code).toEqual(101);
    });
});
