import { apgLogin, apgAirports, apilayerCheck } from './services/index';

const apiProvider = () => ({
    auth: () => new apgLogin,
    info: () => new apgAirports,
    emailCheck: () => new apilayerCheck,
});

export { apiProvider };
