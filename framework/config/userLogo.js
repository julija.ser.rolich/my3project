const userLogo = {
    email: 'julija.ser.rolich@gmail.com',
    password: '123456789',
};
export { userLogo };


// любая новая комбинация name\pass для создания нового пользователя:
const logPass = {
    userName: '18935',
    password: '126fggh8ERty5!',
};
export { logPass };

const logPassForTest = {
    userName: '3334578',
    password: '12345QWERty!',
};
export { logPassForTest };

const logPassNumLet = {
    userName: '12345',
    password: '12345QWERty',
};
export { logPassNumLet };

const logPassLessNum = {
    userName: '12345',
    password: '123QRty',
};
export { logPassLessNum };
