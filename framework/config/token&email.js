const token = '95e9ce8eb51a3586303cb968aeb872be';
export { token };

const emptyToken = '';
export { emptyToken };

const validEmail = 'myvgidemail@ggmma.by';
export { validEmail };

const noValidEmail1 = '4@.by';
export { noValidEmail1 };

const noValidEmail2 = '4mail.by';
export { noValidEmail2 };

const noValidEmail3 = '4@by';
export { noValidEmail3 };
