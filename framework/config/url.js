const urls = {
    airportgap: 'https://airportgap.dev-tester.com',
    bookstore: 'https://bookstore.toolsqa.com',
    emailcheck: 'http://apilayer.net/api',
};
export { urls };

const url = {
    apichallenges: 'https://apichallenges.herokuapp.com/',
}
export { url }
