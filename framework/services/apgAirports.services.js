import supertest from 'supertest';
//import fetch from 'node-fetch';
import { urls } from '../config/index';

const apgAirports = function apgAirports() {
    this.get = async function getAirports() {
        const res = await supertest(urls.airportgap)
            .get('/api/airports')
            .set('Accept', 'application/json');
        return res;
    };
};

export { apgAirports };
