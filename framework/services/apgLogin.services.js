import supertest from 'supertest';
//import fetch from 'node-fetch';
import { userLogo, urls } from '../config/index';

const apgLogin = function apgAirports() {
    this.postToken = async function postToken() {
        const res = await supertest(`${urls.airportgap}`)
            .post('/api/tokens')
            .set('Accept', 'application/json')
            .send(userLogo);
        return res;
    };
};

export { apgLogin };

