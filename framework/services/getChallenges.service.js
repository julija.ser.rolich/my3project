import supertest from 'supertest';
import {url} from "../config/url";

const GetChallenges = function (){
    this.get = async function (){
        const r = await supertest(url.apichallenges).get('/challenges');
        return r;
    }
};

export { GetChallenges };
