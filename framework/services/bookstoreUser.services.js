import supertest from 'supertest';
import { urls } from '../config/index';

// eslint-disable-next-line no-unused-vars
const bookstoreUser = function bookstoreUser(name) {
    this.postUser = async function postUser() {
        const res = await supertest(`${urls.bookstore}`)
            .post('/Account/v1/User')
            .set('Accept', 'application/json')
            .send(name);
        return res;
    };
};
export { bookstoreUser };
