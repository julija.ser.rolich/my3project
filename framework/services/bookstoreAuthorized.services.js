import supertest from 'supertest';
import { urls, logPass, logPassNumLet } from '../config/index';

const authorized = function authorized(name) {
    this.postLogPass = async function postLogPass() {
        const res = await supertest(`${urls.bookstore}`)
            .post('/Account/v1/Authorized')
            .set('Accept', 'application/json')
            .send(name);
        return res;
    };
};
export { authorized };
