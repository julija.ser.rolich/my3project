import supertest from 'supertest';
import fetch from 'node-fetch';
import { urls, token, emptyToken, validEmail, noValidEmail1, noValidEmail2, noValidEmail3 } from '../config/index';


const apilayerCheck = function apilayerCheck(email) {
    this.getAll = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?access_key=${token}&email=${validEmail}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };

    this.getAllnotVal1 = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?access_key=${token}&email=${noValidEmail1}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };

    this.getAllnotVal2 = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?access_key=${token}&email=${noValidEmail2}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };

    this.getAllnotVal3 = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?access_key=${token}&email=${noValidEmail3}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };


    this.getNoAll = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?access_key=&email=${email}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };


    this.getEmpty = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?access_key=${emptyToken}&email=${email}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };

    this.getNoAccess = async function getCheck() {
        const res = await fetch(`${urls.emailcheck}/check?email=${email}&smtp=1&format=1`, { method: 'get' });
        const data = await res.json();
        return data;
    };

};

export { apilayerCheck };
