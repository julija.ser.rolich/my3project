import supertest from 'supertest';
import { urls, logPass, logPassNumLet } from '../config/index';

const generateToken = function generateToken(name) {
    this.postLogPass = async function postLogPass() {
        const res = await supertest(`${urls.bookstore}`)
            .post('/Account/v1/GenerateToken')
            .set('Accept', 'application/json')
            .send(name);
        return res;
    };
};
export { generateToken };
