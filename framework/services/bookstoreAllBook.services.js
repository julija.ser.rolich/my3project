import supertest from 'supertest';
import { urls, logPass, logPassNumLet } from '../config/index';

const allBook = function allBook() {
    this.getAllBook = async function() {
        const res = await supertest(`${urls.bookstore}`)
            .get('/BookStore/v1/Books')
            .set('Accept', 'application/json');
        return res;
    };
};

export { allBook };
